// Contains all the endpoints for our application
// We seperate the routes such that "index.js" only contains information on the server

const express = require('express')
const taskController = require('../controllers/taskControllers')

// Creates a Router instance that functions as a middleware and routing system
// Allow access to HTTP method middlewares that makes it easier to create routes for our application
const router = express.Router()

// Routes
// The routes are responsible for defining the URIs that our client accesses and the corresponsding controller function that will be used when a route is accessed
// They invoke the controller functions from the controller files
// All the business logic is done in the controller

// Route to GET ALL THE TASK
router.get('/', (req, res) => {
    taskController.getAllTasks().then(resultFromController => res.send(resultFromController))
})

// Use "module.exports" to export the router object to use in the "index.js"
module.exports = router