require('dotenv').config()
const express = require('express')
const mongoose = require('mongoose')
// This allows us to use all the routes defined in "taskRoute.js"
const taskRoutes = require('./routes/taskRoutes')

const app = express()
const port = 4000;

// ConnectDB
mongoose.set('strictQuery', false)
mongoose.connect(process.env.MONGO_URI, {
    useNewUrlParser: true,
    useUnifiedTopology: true
})

// Middlewares
app.use(express.json())
app.use(express.urlencoded({extended: true}))

// Routes
app.use('/tasks', taskRoutes)

mongoose.connection.once('open', () => {
    console.log('Connected to MongoDB')
    app.listen(port, () => console.log(`Server is up and running on port ${port}`))
})

mongoose.connection.on('error', error => {
    console.log(error)
})