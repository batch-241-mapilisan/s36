// Controllers contains the functions and business logic of our Express JS application
// Meaning all the operations it can do will be placed in this file

// Uses the "require" directive to allow access to the "Task" model which allows us to access methods to perform CRUD operations
// Allows us to use the contents of the "task.js" file in the "models" folder
const Task = require('../models/task')

// Controller function for GETTING ALL THE TASKS
// Defines the functions to be used in the "taskRoutes.js" file and export these functions
module.exports.getAllTasks = () => {
    return Task.find({}).then(result => {
        return result
    })
}