require('dotenv').config()
const express = require('express')
const mongoose = require('mongoose')
const taskRoutes = require('./routes/taskRoutes')

const app = express()
const PORT = process.env.PORT || 4000

// Connect DB
mongoose.set('strictQuery', false)
mongoose.connect(process.env.MONGO_URI, {
    useNewUrlParser: true,
    useUnifiedTopology: true
})

// Middleware
app.use(express.json())
app.use(express.urlencoded({extended: true}))

// Routes
app.use('/tasks', taskRoutes)


mongoose.connection.once('open', () => {
    console.log('Connected to MongoDB')
    app.listen(PORT, () => console.log(`Server is up and running on port ${PORT}`))
})

mongoose.connection.on('error', error => {
    console.log(error)
})
