const Task = require('../models/task')

// Get All Tasks
// GET
const getAllTasks = async (req, res) => {
    const tasks = await Task.find()

    if (!tasks.length) {
        return res.status(404).json({ message: "No tasks found" })
    }

    res.status(200).json(tasks)
}

// Get Single Task
// GET
const getSingleTask = async (req, res) => {
    const { id } = req.params

    const task = await Task.findById(id)
    if (!task) {
        return res.status(404).json({ message: `Task with ${id} does not exist` })
    }

    res.status(200).json(task)
}

// Create New Task
// POST
const createNewTask = async (req, res) => {
    // Destructure name
    const { name } = req.body

    // Confirm data
    if (!name) {
        return res.status(400).json({ message: "Please input task name" })
    }

    // Check for duplicate
    const duplicate = await Task.findOne({name})
    if (duplicate) {
        return res.status(409).json({ message: "Task already exists" })
    }

    // Create and store new task
    const newTask = await Task.create({name})

    if (newTask) {
        res.status(201).json({ message: `New task ${name} successfully created` })
    } else {
        res.status(400).json({ message: "Invalid task data received" })
    }
}

// Update Task
// PUT
const updateTask = async (req, res) => {
    const { id } = req.params
    const { name, status } = req.body

    // Confirm data
    if (!name || !status) {
        return res.status(400).json({ message: 'Please input all fields' })
    }

    // Check if task exists
    const task = await Task.findById(id)
    
    if (!task) {
        return res.status(404).json({ message: `Task with id ${id} does not exist` })
    }

    // Check for duplicate task name
    const duplicate = await Task.findOne({ name })
    if (duplicate && duplicate._id.toString() !== id) {
        return res.status(409).json({ message: 'Duplicate task detected' })
    }

    task.name = name
    task.status = status

    const updatedTask = await task.save()

    res.status(200).json({ message: `${updatedTask.name} updated` })
}

// Delete Task
// DELETE
const deleteTask = async (req, res) => {
    const { id } = req.params

    if (!id) {
        return res.status(400).json({ message: "Task ID required" })
    }

    const task = await Task.findById(id)

    if (!task) {
        return res.status(400).json({ message: "Task not found" })
    }

    const result = await task.deleteOne()

    const reply = `Task ${result.name} with ID ${result._id} deleted`

    res.status(200).json(reply)
}

module.exports = {
    getAllTasks,
    getSingleTask,
    createNewTask,
    updateTask,
    deleteTask
}
