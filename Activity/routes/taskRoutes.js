const express = require('express')
const {
    getAllTasks,
    getSingleTask,
    createNewTask,
    updateTask,
    deleteTask
} = require('../controllers/taskControllers')
const router = express.Router()

router.route('/')
    .get(getAllTasks)
    .post(createNewTask)

router.route('/:id')
    .get(getSingleTask)
    .put(updateTask)
    .delete(deleteTask)

module.exports = router
